<?php

if (!defined('ABSPATH')) exit;

if (!wp_next_scheduled('ameUpdateFeed')) {
    wp_schedule_event(time(), 'ameEmbedInterval', 'ameUpdateFeed');
}

add_action('ameUpdateFeed', 'ameCronImport', 10);
add_filter('cron_schedules', 'ameAddEverySixHoursCronSchedule');

function ameCronImport(): void
{
    global $wpdb;

    $table = $wpdb->prefix . 'embedFeeds';
    $table_logs = $wpdb->prefix . 'coreLogs';

    $query = "SELECT * FROM `{$table}` WHERE `autoImport` = 'enable'";

    $data = $wpdb->get_results($query);

    foreach ($data as $feed) {
        $keyword = ($feed->categoryPartnerId == "keyword") ? explode("Kw_", $feed->categoryPartner)[1] : "";

        $link = ($feed->partner == 'Partner') ?
            AME_API_CRON . $feed->partner . '/' . $feed->categoryPartner . '/' . get_option('amve-options-page')['auto-import-amount'] . '/' . $feed->categoryPartnerId :
            AME_API_CRON . $feed->partner . '/' . $feed->categoryPartnerId . '/' . get_option('amve-options-page')['auto-import-amount'];

        $curl = curl_init();

        if ($curl) {
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, 'keyword="' . $keyword . '"&jsonData=' . json_encode(explode(',', $feed->data)));
            $out = curl_exec($curl);

            if (strpos('id', $out) >= 0) {
                $res = json_decode($out, true);

                foreach ($res as $item) {
                    if ($item['id'] == '') break;

                    $wpdb->insert($table_logs, [
                        "date" => date("Y-m-d H:i:s"),
                        "type" => "notice",
                        "product" => AME_PRODUCT,
                        "message" => "AUTO IMPORT for " . $feed->partner . " finished at " . date("Y-m-d H:i:s"),
                        "location" => "...." . explode('/plugins/', __FILE__)[1] . ":" . __LINE__
                    ], ["%s", "%s", "%s", "%s", "%s"]);
                }
            }
            curl_close($curl);
        }
    }
    wp_die();
}

function ameAddEverySixHoursCronSchedule($schedules)
{
    $schedules['ameEmbedInterval'] = [
        'interval' => get_option('amve-options-page')['amve-auto-import-frequency'],
        'display' => __('Every 1 hour'),
    ];
    return $schedules;
}
