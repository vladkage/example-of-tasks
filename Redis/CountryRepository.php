<?php

namespace App\Repositories\Country;

use App\Exceptions\DatabaseException;
use App\Models\MySql\Country;
use App\Repositories\BaseRepository;
use App\Repositories\Country\Interfaces\CountryRepositoryInterface;
use App\Repositories\Country\Traits\CountryRepositoryCacheTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cache;
use Exception;

/**
 * Class CountryRepository
 *
 * @package App\Repositories\Country
 */
class CountryRepository extends BaseRepository implements CountryRepositoryInterface
{
    use CountryRepositoryCacheTrait;

    /**
     * CountryRepository constructor
     *
     * @param bool|null $caching
     */
    public function __construct(
        ?bool $caching = true
    )
    {
        $this->caching = !is_null($caching) ? $caching : config('repositories.country.caching');
        $this->perPage = config('repositories.country.perPage');
        $this->cacheTime = config('repositories.country.cacheTime');
    }

    /**
     * @param int|null $id
     * @param string|null $search
     *
     * @return Country|null
     *
     * @throws DatabaseException
     */
    public function findById(
        ?int $id,
        ?string $search = null
    ) : ?Country
    {
        try {
            return Country::query()
                ->with([
                    'regions' => function ($query) use ($search) {
                        $query->when($search, function ($query) use ($search) {
                            $query->whereRaw('lower(name) like (?)', ['%' . strtolower(trim($search)) . '%']);
                        })
                        ->orderBy('name', 'asc');
                    }
                ])
                ->where('id', '=', $id)
                ->first();
        } catch (Exception $exception) {
            throw new DatabaseException(
                trans('exceptions/repository/country.' . __FUNCTION__),
                $exception->getMessage()
            );
        }
    }

    /**
     * @return Country|null
     * 
     * @throws DatabaseException
     */
    public function findByLastPostion() : ?Country
    {
        try {
            return Country::query()
                ->orderBy('position', 'desc')
                ->first();
        } catch (Exception $exception) {
            throw new DatabaseException(
                trans('exceptions/repository/country.' . __FUNCTION__),
                $exception->getMessage()
            );
        }
    }

    /**
     * @return Collection
     *
     * @throws DatabaseException
     */
    public function getAll() : Collection
    {
        if ($this->caching === false) {
            if (method_exists($this, 'clearCache')) {
                $this->clearCache(
                    __FUNCTION__
                );
            }
        }

        try {
            return Cache::remember('countries', $this->cacheTime,
                function () {
                    return Country::query()
                        ->with([
                            'phoneCode'
                        ])
                        ->orderBy('position', 'asc')
                        ->get();
                }
            );
        } catch (Exception $exception) {
            throw new DatabaseException(
                trans('exceptions/repository/country.' . __FUNCTION__),
                $exception->getMessage()
            );
        }
    }

    /**
     * @param string $search
     *
     * @return Collection
     *
     * @throws DatabaseException
     */
    public function getAllBySearch(
        string $search
    ) : Collection
    {
        try {
            return Country::query()
                ->with([
                    'phoneCode'
                ])
                ->whereRaw('lower(name) like (?)', ['%' . strtolower(trim($search)) . '%'])
                ->orderBy('position', 'asc')
                ->get();
        } catch (Exception $exception) {
            throw new DatabaseException(
                trans('exceptions/repository/country.' . __FUNCTION__),
                $exception->getMessage()
            );
        }
    }

    /**
     * @return Collection
     *
     * @throws DatabaseException
     */
    public function getAllWithoutRegions() : Collection
    {
        if ($this->caching === false) {
            if (method_exists($this, 'clearCache')) {
                $this->clearCache(
                    __FUNCTION__
                );
            }
        }

        try {
            return Cache::remember('countries.without.regions', $this->cacheTime,
                function () {
                    return Country::query()
                        ->where('visible', '=', true)
                        ->orderBy('name', 'asc')
                        ->get();
                }
            );
        } catch (Exception $exception) {
            throw new DatabaseException(
                trans('exceptions/repository/country.' . __FUNCTION__),
                $exception->getMessage()
            );
        }
    }

    /**
     * @param int|null $page
     * @param int|null $perPage
     *
     * @return LengthAwarePaginator
     *
     * @throws DatabaseException
     */
    public function getAllPaginated(
        ?int $page = null,
        ?int $perPage = null
    ) : LengthAwarePaginator
    {
        if ($this->caching === false) {
            if (method_exists($this, 'clearCache')) {
                $this->clearCache(
                    __FUNCTION__,
                    $page,
                    $perPage ? $perPage : $this->perPage
                );
            }
        }

        try {
            return Cache::remember('countries.' . $page . '.perPage.' . ($perPage ? $perPage : $this->perPage), $this->cacheTime,
                function () use ($page, $perPage) {
                    return Country::query()
                        ->with([
                            'phoneCode'
                        ])
                        ->withCount(
                            'users'
                        )
                        ->orderBy('position', 'asc')
                        ->paginate($perPage ? $perPage : $this->perPage, ['*'], 'page', $page);
                }
            );
        } catch (Exception $exception) {
            throw new DatabaseException(
                trans('exceptions/repository/country.' . __FUNCTION__),
                $exception->getMessage()
            );
        }
    }

    /**
     * @param string $search
     * @param int|null $page
     * @param int|null $perPage
     *
     * @return LengthAwarePaginator
     *
     * @throws DatabaseException
     */
    public function getAllBySearchPaginated(
        string $search,
        ?int $page = null,
        ?int $perPage = null
    ) : LengthAwarePaginator
    {
        try {
            return Country::query()
                ->with([
                    'phoneCode'
                ])
                ->whereRaw('lower(name) like (?)', ['%' . strtolower(trim($search)) . '%'])
                ->orderBy('position', 'asc')
                ->paginate($perPage ? $perPage : $this->perPage, ['*'], 'page', $page);
        } catch (Exception $exception) {
            throw new DatabaseException(
                trans('exceptions/repository/country.' . __FUNCTION__),
                $exception->getMessage()
            );
        }
    }

    /**
     * @return Collection
     *
     * @throws DatabaseException
     */
    public function getFullWithRegionsAndCities() : Collection
    {
        if ($this->caching === false) {
            if (method_exists($this, 'clearCache')) {
                $this->clearCache(
                    __FUNCTION__
                );
            }
        }

        try {
            return Cache::remember('countries.regions.cities', $this->cacheTime,
                function () {
                    return Country::query()
                        ->with([
                            'regions.cities'
                        ])
                        ->where('visible', '=', true)
                        ->orderBy('name', 'asc')
                        ->get();
                }
            );
        } catch (Exception $exception) {
            throw new DatabaseException(
                trans('exceptions/repository/country.' . __FUNCTION__),
                $exception->getMessage()
            );
        }
    }

    /**
     * @param string $name
     * @param string $countryCode
     * @param bool|null $visible
     * @param int|null $position
     *
     * @return Country|null
     *
     * @throws DatabaseException
     */
    public function store(
        string $name,
        string $countryCode,
        ?bool $visible = true,
        ?int $position = 1
    ) : ?Country
    {
        try {
            return Country::create([
                'name'         => trim($name),
                'code'         => generateCodeByName(trim($name)),
                'country_code' => $countryCode,
                'visible'      => $visible,
                'position'     => $position
            ]);
        } catch (Exception $exception) {
            throw new DatabaseException(
                trans('exceptions/repository/country.' . __FUNCTION__),
                $exception->getMessage()
            );
        }
    }

    /**
     * @param Country $country
     * @param string|null $name
     * @param string|null $countryCode
     * @param bool|null $visible
     * @param int|null $position
     *
     * @return Country
     *
     * @throws DatabaseException
     */
    public function update(
        Country $country,
        ?string $name,
        ?string $countryCode,
        ?bool $visible,
        ?int $position
    ) : Country
    {
        try {
            $country->update([
                'name'     => $name ? trim($name) : $country->name,
                'code'     => $name ? generateCodeByName($name) : $country->code,
                'visible'  => !is_null($visible) ? $visible : $country->visible,
                'position' => $position ? $position : $country->position
            ]);

            return $country;
        } catch (Exception $exception) {
            throw new DatabaseException(
                trans('exceptions/repository/country.' . __FUNCTION__),
                $exception->getMessage()
            );
        }
    }

    /**
     * @param Country $country
     * @param int $position
     *
     * @throws DatabaseException
     */
    public function updatePosition(
        Country $country,
        int $position
    ) : void
    {
        try {
            $country->update([
                'position' => $position
            ]);
        } catch (Exception $exception) {
            throw new DatabaseException(
                trans('exceptions/repository/country.' . __FUNCTION__),
                $exception->getMessage()
            );
        }
    }

    /**
     * @param Country $country
     *
     * @return bool|null
     *
     * @throws DatabaseException
     */
    public function delete(
        Country $country
    ) : ?bool
    {
        try {
            return $country->delete();
        } catch (Exception $exception) {
            throw new DatabaseException(
                trans('exceptions/repository/country.' . __FUNCTION__),
                $exception->getMessage()
            );
        }
    }
}