<?php

namespace App\Exceptions;

/**
 * Class MicroserviceException
 *
 * @package App\Exceptions
 */
class MicroserviceException extends BaseException
{
    /**
     * Exception error type constants
     */
    const TYPE_CONNECTION_ERROR = 'connection';
    const TYPE_AUTH_ERROR = 'auth';
    const TYPE_VALIDATION_ERROR = 'validation';
    const TYPE_SERVER_ERROR = 'server';
    const TYPE_UNIDENTIFIED_ERROR = 'unidentified';

    /**
     * Exception appearances constants
     */
    const APPEARANCE_MICROSERVICE_MEDIA = 'microservice_media';

    /**
     * Validation errors
     *
     * @var array|null
     */
    protected ?array $validationErrors = null;

    /**
     * MicroserviceException constructor
     *
     * @param string $errorAppearance
     * @param string $errorType
     * @param int $errorCode
     * @param string|null $explainErrorMessage
     * @param string|null $systemErrorMessage
     * @param array|null $validationErrors
     */
    public function __construct(
        string $errorAppearance,
        string $errorType,
        int $errorCode,
        ?string $explainErrorMessage = null,
        ?string $systemErrorMessage = null,
        ?array $validationErrors = null
    )
    {
        /**
         * Setting probable validation errors data
         */
        $this->validationErrors = $validationErrors;

        /**
         * Execute base exception constructor
         */
        parent::__construct(
            $explainErrorMessage,
            $errorCode,
            $systemErrorMessage,
            $errorAppearance,
            $errorType
        );
    }

    /**
     * @return array|null
     */
    public function getValidationErrors() : ?array
    {
        return $this->validationErrors;
    }
}
