<?php

namespace Tests\Unit\Services\Order;

use App\Exceptions\BaseException;
use App\Models\MySql\Order\Order;
use App\Models\MySql\User\User;
use App\Repositories\Order\OrderRepository;
use App\Services\Order\OrderService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class OrderServiceTest extends TestCase
{
    use DatabaseTransactions;

    protected OrderService $orderService;
    protected OrderRepository $orderRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->orderService = new OrderService();
        $this->orderRepository = new OrderRepository();
    }

    public function testCreateOrderWithValidData(): void
    {
        $buyer = User::factory()->create();

        $paymentMethod = new PaymentMethod();
        $orderItems = [];

        $order = $this->orderService->createOrder($buyer, $paymentMethod, $orderItems);

        $this->assertInstanceOf(Order::class, $order);
    }

    public function testCreateOrderWithInvalidData(): void
    {
        $buyer = new User();
        $paymentMethod = new PaymentMethod();
        $orderItems = [];

        $this->expectException(BaseException::class);
        $this->orderService->createOrder($buyer, $paymentMethod, $orderItems);
    }

    public function testGetPaymentFeeAmount(): void
    {
        $amount = 100.00;
        $paymentFee = 5;

        $result = $this->orderService->getPaymentFeeAmount($amount, $paymentFee);

        $this->assertEquals(5.00, $result);
    }

    public function testGetVat(): void
    {
        $user = User::factory()->create([
            'country_id' => 1,
        ]);

        $taxRule = TaxRule::factory()->create([
            'country_id' => $user->country_id,
            'tax_rate' => 20,
        ]);

        $amount = 100.00;
        $paymentFee = 5.00;

        $vat = $this->orderService->getVat($taxRule, $amount, $paymentFee);

        $expectedVat = round(($amount + $paymentFee) / 100 * $taxRule->tax_rate, 2);

        $this->assertEquals($expectedVat, $vat);
    }

    public function testGetOrderPaymentUrlByMethod(): void
    {
        $paymentMethod = PaymentMethod::factory()->create([
            'code' => 'paypal',
        ]);

        $orderInvoice = OrderInvoice::factory()->create([
            'id',
            'order_item_id',
            'type',
            'status'
        ]);

        $mockedPayPalService->shouldReceive('getOrderPaymentUrl')
            ->once()
            ->with($orderInvoice)
            ->andReturn('https://paypal.example.com/payment/url'); 

        $paymentUrl = $this->orderService->getOrderPaymentUrlByMethod($paymentMethod, $orderInvoice);
        $this->assertEquals('https://paypal.example.com/payment/url', $paymentUrl);
    }
}
