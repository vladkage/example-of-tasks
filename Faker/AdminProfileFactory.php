<?php

namespace Database\Factories\MySql;

use App\Models\MySql\AdminProfile;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class AdminProfileFactory
 *
 * @package Database\Factories\MySql
 */
class AdminProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AdminProfile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name'  => $this->faker->lastName,
            'last_name'   => $this->faker->firstName,
            'full_access' => $this->faker->boolean
        ];
    }
}
