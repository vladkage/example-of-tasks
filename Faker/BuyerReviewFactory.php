<?php

namespace Database\Factories\MySql\Review;

use App\Models\MySql\Review\BuyerReview;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class BuyerReviewFactory
 *
 * @package Database\Factories\MySql\Review
 */
class BuyerReviewFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BuyerReview::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'text' => $this->faker->realText(255)
        ];
    }
}
