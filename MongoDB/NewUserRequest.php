<?php

namespace App\Models\MongoDb\User;

use App\Lists\AccountStatus\AccountStatusList;
use App\Lists\AccountStatus\AccountStatusListItem;
use App\Lists\Language\LanguageList;
use App\Lists\Language\LanguageListItem;
use App\Lists\RequestFieldStatus\RequestFieldStatusList;
use App\Lists\RequestFieldStatus\RequestFieldStatusListItem;
use App\Lists\RequestStatus\RequestStatusList;
use App\Lists\RequestStatus\RequestStatusListItem;
use App\Lists\ToastMessageType\ToastMessageTypeList;
use App\Lists\ToastMessageType\ToastMessageTypeListItem;
use App\Models\MongoDb\Suggestion\LocationSuggestion;
use App\Models\MySql\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Jenssegers\Mongodb\Eloquent\Model;

/**
 * App\Models\MongoDb\NewUserRequest
 *
 * @property string $_id
 * @property int $user_id
 * @property int $account_status_id
 * @property int $account_status_status_id
 * @property string $username
 * @property int $username_status_id
 * @property string $birth_date
 * @property int $birth_date_status_id
 * @property string $description
 * @property int $description_status_id
 * @property int $voice_sample_status_id
 * @property int $avatar_status_id
 * @property int $background_status_id
 * @property int $album_status_id
 * @property int $toast_message_type_id
 * @property string $toast_message_text
 * @property int $request_status_id
 * @property int $location_suggestion_id
 * @property int $admin_id
 * @property int $language_id
 * @property Carbon $created_at
 * @property-read User $user
 * @property-read User $admin
 * @property-read LocationSuggestion $locationSuggestion
 *
 */
class NewUserRequest extends Model
{
    /**
     * Database connection type
     *
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * Database collection name
     *
     * @var string
     */
    protected $collection = 'new_user_requests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'account_status_id', 'account_status_status_id', 'username', 'username_status_id',
        'birth_date', 'birth_date_status_id', 'description', 'description_status_id', 'voice_sample_status_id',
        'avatar_status_id', 'background_status_id', 'album_status_id', 'toast_message_type_id',
        'toast_message_text', 'request_status_id', 'location_suggestion_id', 'admin_id', 'language_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'birth_date' => 'datetime'
    ];

    //--------------------------------------------------------------------------
    // Belongs to relations

    /**
     * @return BelongsTo
     */
    public function admin() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Jenssegers\Mongodb\Relations\BelongsTo
     */
    public function locationSuggestion() : \Jenssegers\Mongodb\Relations\BelongsTo
    {
        return $this->belongsTo(LocationSuggestion::class);
    }

    //--------------------------------------------------------------------------
    // Lists accessors

    /**
     * @return AccountStatusListItem|null
     */
    public function getAccountStatus() : ?AccountStatusListItem
    {
        return AccountStatusList::getItem(
            $this->account_status_id
        );
    }

    /**
     * @return RequestFieldStatusListItem|null
     */
    public function getAccountStatusStatus() : ?RequestFieldStatusListItem
    {
        return RequestFieldStatusList::getItem(
            $this->account_status_status_id
        );
    }

    /**
     * @return RequestFieldStatusListItem|null
     */
    public function getUsernameStatus() : ?RequestFieldStatusListItem
    {
        return RequestFieldStatusList::getItem(
            $this->username_status_id
        );
    }

    /**
     * @return RequestFieldStatusListItem|null
     */
    public function getBirthdateStatus() : ?RequestFieldStatusListItem
    {
        return RequestFieldStatusList::getItem(
            $this->birth_date_status_id
        );
    }

    /**
     * @return RequestFieldStatusListItem|null
     */
    public function getDescriptionStatus() : ?RequestFieldStatusListItem
    {
        return RequestFieldStatusList::getItem(
            $this->description_status_id
        );
    }

    /**
     * @return RequestFieldStatusListItem|null
     */
    public function getVoiceSampleStatus() : ?RequestFieldStatusListItem
    {
        return RequestFieldStatusList::getItem(
            $this->voice_sample_status_id
        );
    }

    /**
     * @return RequestFieldStatusListItem|null
     */
    public function getAvatarStatus() : ?RequestFieldStatusListItem
    {
        return RequestFieldStatusList::getItem(
            $this->avatar_status_id
        );
    }

    /**
     * @return RequestFieldStatusListItem|null
     */
    public function getBackgroundStatus() : ?RequestFieldStatusListItem
    {
        return RequestFieldStatusList::getItem(
            $this->background_status_id
        );
    }

    /**
     * @return RequestFieldStatusListItem|null
     */
    public function getAlbumStatus() : ?RequestFieldStatusListItem
    {
        return RequestFieldStatusList::getItem(
            $this->album_status_id
        );
    }

    /**
     * @return ToastMessageTypeListItem|null
     */
    public function getToastMessageType() : ?ToastMessageTypeListItem
    {
        return ToastMessageTypeList::getItem(
            $this->toast_message_type_id
        );
    }

    /**
     * @return RequestStatusListItem|null
     */
    public function getRequestStatus() : ?RequestStatusListItem
    {
        return RequestStatusList::getItem(
            $this->request_status_id
        );
    }

    /**
     * @return LanguageListItem|null
     */
    public function getLanguage() : ?LanguageListItem
    {
        return LanguageList::getItem(
            $this->language_id
        );
    }
}