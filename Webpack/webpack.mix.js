const mix = require('laravel-mix');

mix.js('resources/js/main.js', 'public/js')
    .js('resources/js/layouts/appLayout.js', 'public/js/layouts')
    .js('resources/js/layouts/mainLayout.js', 'public/js/layouts')
    .js('resources/js/pages/index.js', 'public/js/pages')
    .js('resources/js/pages/auth/signup.js', 'public/js/pages/auth')
    .js('resources/js/pages/games/scene.js', 'public/js/pages/games')
    .js('resources/js/pages/games/embed.js', 'public/js/pages/games')
    .js('resources/js/pages/games/embedClicker.js', 'public/js/pages/games')
    .js('resources/js/pages/games/embedClickerCritRate.js', 'public/js/pages/games')
    .js('resources/js/pages/games/index.js', 'public/js/pages/games')
    .js('resources/js/pages/games/show.js', 'public/js/pages/games')
    .js('resources/js/pages/profile/index.js', 'public/js/pages/profile')
    .js('resources/js/pages/profile/show.js', 'public/js/pages/profile')
    .js('resources/js/pages/history/index.js', 'public/js/pages/history')
    .js('resources/js/pages/album/index.js', 'public/js/pages/album')
    .js('resources/js/pages/album/show.js', 'public/js/pages/album')
    .js('resources/js/pages/home/index.js', 'public/js/pages/home')
    .js('resources/js/pages/ai-generated/index.js', 'public/js/pages/ai-generated')
    .js('resources/js/pages/tag/categories.js', 'public/js/pages/tag')

    .postCss('resources/css/admin.css', 'public/css', [])
    .postCss('resources/css/main.css', 'public/css', [])
    .postCss('resources/css/auth.css', 'public/css', [])
    .postCss('resources/css/embed.css', 'public/css', [])
    .postCss('resources/css/embed-clicker.css', 'public/css', [])
    .postCss('resources/css/embed-clicker-critical-rate.css', 'public/css', [])
    .postCss('resources/css/game-type.css', 'public/css', [])
mix.disableNotifications();
