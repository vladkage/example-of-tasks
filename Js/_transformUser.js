const user = {

    single (user) {
        return {
            id              : user.id || '0',
            login           : user.login || "# Login",
            email           : user.email || "example@domain.com",
            subscriptions   : this.subscriptions(user.subscriptions),
            subscribers     : this.subscribers(user.subscribers),
            socials         : user.socials,
            recentVisits    : user.recent_visits,
            blockedList     : user.block_list,
            giphyIcons      : user.giphy_icons
        }
    },

    subscribers: (users) => {
        return users.map( user => {
            return {
                id            : user.id,
                username      : user.username,
                first_name    : user.first_name,
                last_name     : user.last_name,
                gender        : user.gender,
                subscriptions : user.subscriptions,
                subscribers   : user.subscribers,
                languages     : user.languages,
                avatar        : user.avatar,
                status        : user.status
            }
        })
    },

    subscriptions (users) { return this.subscribers(users) },
}
export default user