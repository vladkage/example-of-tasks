import Vue from 'vue'
import App from './App.vue'
import './assets/css/app.scss'
import VueMeta from "vue-meta";
import {createStore} from './store'
import {createRouter} from './router'

Vue.config.productionTip = false
Vue.use(VueMeta)

export async function createApp({
                                    beforeApp = () => {
                                    },
                                    afterApp = () => {
                                    }
                                } = {}) {

    const router = createRouter()
    const store = createStore()

    await beforeApp({
        router,
        store,
    })

    const app = new Vue({
        store,
        router,
        render: h => h(App)
    })

    const result = {
        app,
        router,
        store,
    }

    await afterApp(result)

    return result
}
