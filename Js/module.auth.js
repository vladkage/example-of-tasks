import api from 'src/api/api'
import * as mutypes from "src/store/mutation.types"
import transform from "src/store/transform/transform";

export default {
    state: {
        auth: {
            token: localStorage.getItem('token') || '',
            user_profile: {},
            user_grabbed: false,
            data: {
                email: '',
                password: '',
                remember: false,
                recaptcha_token: '',
                success_message: '',
            },
            errors: {
                email: '',
                password: '',
                message: '',
            },
        },
    },

    mutations: {
        [mutypes.AUTH_SUCCESS](state, token) {
            localStorage.setItem('token', token)
            state.auth.token = token
        },
        [mutypes.AUTH_ERRORS](state, errors) {
            state.auth.errors.email = errors.email ? errors.email[0] : ''
            state.auth.errors.password = errors.password ? errors.password[0] : ''
            state.auth.errors.message = errors.message ? errors.message : ''
        },
        [mutypes.SET_AUTH_DATA_EMAIL]: (state, value) => state.auth.data.email = value,
        [mutypes.SET_AUTH_DATA_PASSWORD]: (state, value) => state.auth.data.password = value,
        [mutypes.SET_AUTH_DATA_REMEMBER]: (state, value) => state.auth.data.remember = value,
        [mutypes.SET_AUTH_DATA_RECAPTCHA_TOKEN]: (state, value) => state.auth.data.recaptcha_token = value,
        [mutypes.SET_AUTH_DATA_SUCCESS_MESSAGE]: (state, value) => state.auth.data.success_message = value,
        [mutypes.SET_AUTH_ERRORS_EMAIL]: (state, value) => state.auth.errors.email = value,
        [mutypes.SET_AUTH_ERRORS_PASSWORD]: (state, value) => state.auth.errors.password = value,
        [mutypes.SET_AUTH_ERRORS_MESSAGE]: (state, value) => state.auth.errors.message = value,
        [mutypes.AUTH_USER_PROFILE](state, profile) {
            state.auth.user_profile = transform.profile.authUser(profile)
        },
        [mutypes.AUTH_USER_GRABBED]: (state, value) => state.auth.user_grabbed = value,
        [mutypes.AUTH_CLEAR](state) {
            localStorage.removeItem('token')
            state.auth.token = ''
            state.auth.user_profile = {}
            state.auth.user_grabbed = false
        },
        [mutypes.SET_AUTH_USER_FOLLOWERS](state, followers) {
            state.auth.user_profile.followers = transform.follower.multiple(followers)
        },
        [mutypes.SET_AUTH_USER_FOLLOWING](state, following) {
            state.auth.user_profile.following = transform.follower.multiple(following)
        },
        [mutypes.REMOVE_FROM_BLOCKED_LIST](state, blocked_user_id) {
            state.auth.user_profile.block_list = state.auth.user_profile.block_list.filter(item => item.blocked_user_id !== blocked_user_id);
        },
        [mutypes.ADD_TO_BLOCKED_LIST](state, blocked_user_id) {
            state.auth.user_profile.block_list = [...state.auth.user_profile.block_list, {blocked_user_id: blocked_user_id}];
        },
        [mutypes.ADD_TO_FAVORITE_GIPHY_ICONS](state, gif) {
            state.auth.user_profile.giphy_icons = [...state.auth.user_profile.giphy_icons, gif];
        },
        [mutypes.REMOVE_FROM_FAVORITE_GIPHY_ICONS](state, id) {
            state.auth.user_profile.giphy_icons = state.auth.user_profile.giphy_icons.filter(item => item.external_id!==id);
        },
    },

    actions: {

        login: async ({commit, dispatch, getters}) => {
            try {
                const resp = await api.guest.auth.login(getters.getAuthData)
                commit(mutypes.AUTH_SUCCESS, resp.data.token)
                const profile = await dispatch('grabAuthUserProfile')
                commit(mutypes.AUTH_USER_GRABBED, !!profile)
                commit(mutypes.SET_AUTH_DATA_EMAIL, '')
                commit(mutypes.SET_AUTH_DATA_PASSWORD, '')
                commit(mutypes.AUTH_ERRORS, {})
                return profile
            } catch (e) {
                commit(mutypes.AUTH_ERRORS, e.response.data.errors)
                throw new Error('### NO AUTH')
            }
        },

        logout: async ({commit}) => {
            try {
                await api.general.auth.logout()
                commit(mutypes.AUTH_CLEAR)
                return true
            } catch {
                return false
            }
        },

        passwordResetInit: async ({getters, commit}) => {
            try {
                const data = {
                    email: getters.getAuthDataEmail,
                    'g-recaptcha-response': getters.getAuthDataRecaptchaToken,
                }
                const resp = await api.guest.auth.passwordResetInit(data)
                commit(mutypes.SET_AUTH_DATA_SUCCESS_MESSAGE, resp.data.message)
                return true
            } catch {
                return false
            }
        },

        passwordReset: async  ({commit}, data) => {
            try {
                const resp = await api.guest.auth.passwordReset(data)
                commit(mutypes.SET_AUTH_DATA_SUCCESS_MESSAGE, resp.data.message)
                return resp.data
            } catch (e) {
                return e.response.data
            }
        },

        grabAuthUserProfile: async ({commit, getters}, params = null) => {
            try {

                !params && (params = {
                    followings: true,
                    followers: true,
                    activities: true,
                    favorite_vybes: true,
                    favorite_activities: true,
                    block_list: true,
                    personality_traits: true,
                    languages: true,
                    vybes: true,
                    giphy_icons: true,
                    visited_users: true,
                    cache: 'clear'
                })

                commit(mutypes.AUTH_USER_GRABBED, false)
                if (!getters.isLoggedIn) {
                    commit(mutypes.AUTH_CLEAR)
                    return null
                }
                const resp = await api.general.auth.user(params)

                commit(mutypes.AUTH_USER_PROFILE, resp.data)
                commit(mutypes.AUTH_USER_GRABBED, true)

                return getters.authUserProfile
            } catch (e) {
                e.response && 401 === e.response.status && commit(mutypes.AUTH_CLEAR)
                return null
            }
        },

        setAuthUserStateStatus: async ({}, id) => {
            try {
                return await api.general.auth.userSetStateStatus(id)
            } catch (e) {
                return false;
            }
        },
        setAuthUserLanguage: async ({}, id) => {
            try {
                return await api.general.auth.userSetLanguage(id)
            } catch (e) {
                return false;
            }
        },
        setAuthUserCurrency: async ({}, id) => {
            try {
                return await api.general.auth.userSetCurrency(id)
            } catch (e) {
                return false;
            }
        },

        $_moduleAuth_followUnfollow: async ({dispatch}, params) => {
            try {
                const resp = await api.guest.auth[params.action].subscription(params.id)
                200 === resp.status && dispatch('_refreshAuthUser')
                return resp.data
            } catch (e) {
                console.error(e.response)
                return null
            }
        },
        followUser: ({dispatch}, id) => dispatch('$_moduleAuth_followUnfollow', {action: 'attach', id: id}),
        unfollowUser: ({dispatch}, id) => dispatch('$_moduleAuth_followUnfollow', {action: 'detach', id: id}),

        refreshAuthUserFollowers: async ({commit, getters}) => {
            try {
                const resp = await api.general.auth.userSubscribers()
                commit(mutypes.SET_AUTH_USER_FOLLOWERS, resp.data.followers)
                return getters.authUserFollowers
            } catch (e) {
                console.log(e.response ? e.response : e)
                return undefined
            }
        },

        refreshAuthUserFollowing: async ({commit, getters}) => {
            try {
                const resp = await api.general.auth.userSubscriptions()
                commit(mutypes.SET_AUTH_USER_FOLLOWING, resp.data.following)
                return getters.authUserFollowing
            } catch (e) {
                console.log(e.response ? e.response : e)
                return undefined
            }
        },

        attachFavoriteVybe: async ({commit}, id) => {
            try {
                const resp = await api.general.auth.attach.favoriteVybe(id)
                return resp.status
            } catch (e) {
                console.error('### CAN\'T ATTACH FAVORITE VYBE', e)
                return false
            }
        },

        detachFavoriteVybe: async ({commit}, id) => {
            try {
                const resp = await api.general.auth.detach.favoriteVybe(id)
                return resp.status
            } catch (e) {
                console.error('### CAN\'T DETACH FAVORITE VYBE', e)
                return false
            }
        },

        attachFavoriteActivity: async ({dispatch}, activity_id) => {
            try {
                const resp = await api.general.auth.attach.favoriteActivity(activity_id);
                if (resp.status === 200) {
                    dispatch('grabAuthUserProfile', {favorite_activities: true, cache: 'clear'});
                }
                return resp.status
            } catch (e) {
                console.error('### CAN\'T ATTACH FAVORITE ACTIVITY', e)
                return false
            }
        },

        detachFavoriteActivity: async ({dispatch}, activity_id) => {
            try {
                const resp = await api.general.auth.detach.favoriteActivity(activity_id);
                if (resp.status === 200) {
                    dispatch('grabAuthUserProfile', {favorite_activities: true, cache: 'clear'});
                }
                return resp.status;
            } catch (e) {
                console.error('### CAN\'T DETACH FAVORITE ACTIVITY', e);
                return false;
            }
        },

        attachFavoriteActivities: async ({dispatch}, ids) => {
            try {
                const resp = await api.general.auth.attach.favoriteActivities(ids);
                if (resp.status === 200) {
                    dispatch('grabAuthUserProfile', {favorite_activities: true, cache: 'clear'});
                }
                return resp.status;
            } catch (e) {
                console.error('### CAN\'T ATTACH FAVORITE ACTIVITIES', e);
                return false;
            }
        },

        visit: async ({dispatch}, id) => {
            try {
                await api.users.visit(id);
                dispatch('refreshAuthUserRecentVisits');
            } catch (e) {
                console.error(e);
            }
        },

        _refreshAuthUser: async ({commit}) => {
            try {
                const resp = await api.general.auth.user({ cache: 'clear' })
                commit(mutypes.AUTH_USER_PROFILE, resp.data)
                return true
            } catch {
                return false
            }
        },

        _blockList: async ({dispatch}, params) => {
            try {
                const resp = await api.general.auth[params.action].blockedUser(params.id);
                200 === resp.status && dispatch('_refreshAuthUser')
                return resp
            } catch (e) {
                console.error(e)
                return false
            }
        },

        blockUser: ({dispatch}, id) => dispatch('_blockList', {action: 'attach', id: id}),
        unblockUser: ({dispatch}, id) => dispatch('_blockList', {action: 'detach', id: id}),

        addGiphyIconToFavorites: async ({commit}, gif) => {
            try {
                const resp = await api.giphy_icons.attachGipyIcon(gif);
                if (200 === resp.status) {
                    commit('ADD_TO_FAVORITE_GIPHY_ICONS', resp.data['giphy_icon']);
                    return resp;
                }
            } catch (e) {
                console.error(e);
                return false;
            }
        },

        removeGiphyIconFromFavorites: async ({commit}, gif) => {
            try {
                const resp = await api.giphy_icons.detachGipyIcon(gif);
                if (resp.status === 200) {
                    commit('REMOVE_FROM_FAVORITE_GIPHY_ICONS', gif.id);
                    return resp;
                }
            } catch (e) {
                console.error(e);
                return false;
            }
        },
    },

    getters: {
        isLoggedIn: state => !!state.auth.token,
        authUserProfile: state => state.auth.user_profile,
        authUserGrabbed: state => state.auth.user_grabbed,
        authUserFollowing: state => state.auth.user_profile.following,
        authUserFollowers: state => state.auth.user_profile.followers,

        getAuthData: state => state.auth.data,
        getAuthDataEmail: state => state.auth.data.email,
        getAuthDataPassword: state => state.auth.data.password,
        getAuthDataRemember: state => state.auth.data.remember,
        getAuthDataRecaptchaToken: state => state.auth.data.recaptcha_token,
        getAuthDataSuccessMessage: state => state.auth.data.success_message,
        getAuthErrorsEmail: state => state.auth.errors.email,
        getAuthErrorsPassword: state => state.auth.errors.password,
        getAuthErrorsMessage: state => state.auth.errors.message,
    },
}
